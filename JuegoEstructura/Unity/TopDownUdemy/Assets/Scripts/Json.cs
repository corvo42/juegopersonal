﻿using System.IO;
using UnityEngine;

public class Json : MonoBehaviour {
    string path = @"C:\Users\rfria\Desktop\fichero.json";

    void Start () {
       
    }
    public void Serializar()
    {
        PlayerSettings playerSave = new PlayerSettings()
        {
            position = transform.position
        };

        if (!File.Exists(path))
        {
             File.Create(path);
        }
       
        File.WriteAllText(path, JsonUtility.ToJson(playerSave));
    }

    public void Deserializar()
    {
        PlayerSettings player = JsonUtility.FromJson<PlayerSettings>(File.ReadAllText(path));
        transform.position = player.position;

    }

}

class PlayerSettings
{
    public Vector3 position;
}
